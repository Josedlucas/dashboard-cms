
$(document).ready(function() {

  $( ".table-icon" ).on( "mouseenter", function() {
    let change = $(this).data('change');
    $(this).attr('src',change);
  }).on( "mouseleave", function() {
    let init = $(this).data('init');
    $(this).attr('src',init);
  });
  
});