$(document).ready(function() {
    
    $('#sidebarCollapse').on('click', function(e) {
        e.preventDefault();
        $(this).addClass(function(index, currentClass) {
            if (currentClass != 'navbar-btn active') {
                $('#sidebar').addClass('active');
                $(this).addClass('active');
                $('#sidebarNoti').removeClass('active');
            }else{
                $('#sidebar').removeClass('active');
                $(this).removeClass('active');
                $('#sidebarNoti').removeClass('active');
                $('.overlay').removeClass('active');
                $('.sidebarCollapseNoti').removeClass('active');
            }
            $('#sidebar').addClass(function(index, currentClass) {
                if ( currentClass == "active" ) {
                    $('#body').addClass('scrollOculto');
                }else{
                    $('#body').removeClass('scrollOculto');
                }
            });
        });
    });
    

    $('.sidebarCollapseNoti').on('click', function(e) {
        e.preventDefault();
        $(this).addClass(function(index, currentClass) {
            if ($(window).width() <= '720') {
                if (currentClass != 'sidebarCollapseNoti active') {
                    $('#sidebarNoti').addClass('active');
                    $(this).addClass('active');
                    $('#sidebar').removeClass('active');
                    $('#sidebarCollapse').addClass('active');
                    $('.overlay').addClass('active');
                }else{
                    $('#sidebarNoti').removeClass('active');
                    $(this).removeClass('active');
                    $('#sidebar').removeClass('active');
                    $('#sidebarCollapse').removeClass('active');
                    $('.overlay').removeClass('active');
                }
                $('#sidebarNoti').addClass(function(index, currentClass) {
                    if ( currentClass == "active" ) {
                        $('#body').addClass('scrollOculto');
                    }else{
                        $('#body').removeClass('scrollOculto');
                    }
                });
            }else{
                if (currentClass != 'sidebarCollapseNoti active') {
                    $('#sidebarNoti').addClass('active');
                    $(this).addClass('active');
                    $('#sidebar').removeClass('active');
                    $('.overlay').addClass('active');
                }else{
                    $('#sidebarNoti').removeClass('active');
                    $(this).removeClass('active');
                    $('#sidebar').removeClass('active');
                }
            }
        });
    });
    $('#dismiss, .overlay, #cerrar').on('click', function (e) {
        e.preventDefault();
        $('#sidebarNoti').removeClass('active');
        $('.overlay').removeClass('active');
        $('.sidebarCollapseNoti').removeClass('active');
    });
});