/* To avoid CSS expressions while still supporting IE 7 and IE 6, use this script */
/* The script tag referencing this file must be placed before the ending body tag. */

/* Use conditional comments in order to target IE 7 and older:
	<!--[if lt IE 8]><!-->
	<script src="ie7/ie7.js"></script>
	<!--<![endif]-->
*/

(function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'cm-smartPhone\'">' + entity + '</span>' + html;
	}
	var icons = {
		'cm-tiempo': '&#xe900;',
		'cm-agregar-carro': '&#xe901;',
		'cm-agregar-carro-rojo': '&#xe904;',
		'cm-agregar-rojo-1': '&#xe908;',
		'cm-agregar-usuario': '&#xe909;',
		'cm-agregar-usuario-rojo': '&#xe90a;',
		'cm-campana': '&#xe90c;',
		'cm-campana-roja': '&#xe90d;',
		'cm-casa': '&#xe910;',
		'cm-check': '&#xe911;',
		'cm-dolar': '&#xe913;',
		'cm-empresa': '&#xe915;',
		'cm-error': '&#xe916;',
		'cm-flecha-abajo-rojo': '&#xe918;',
		'cm-procentaje': '&#xe919;',
		'cm-reporte-rojo': '&#xe91b;',
		'cm-reportes': '&#xe91d;',
		'0': 0
		},
		els = document.getElementsByTagName('*'),
		i, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		c = el.className;
		c = c.match(/cm-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
}());
